import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpResponse } from '@angular/common/http';

import { ReservationService } from '../services/reservation.service';
import { KpiComponent } from './kpi.component';

describe('StaticPriceComponent', () => {
  let fixture: KpiComponent;
  let ReservationServiceMock: any;
  let OrderServiceMock:any
  let response: HttpResponse<any>;
  beforeEach(() => {
    ReservationServiceMock = {
      getAllReservationsByDateAsc: jest.fn(() => of(response)),
      getAllReservationsByMonth: jest.fn(() => of(response)),
      getAllReservationsByYear: jest.fn(() => of(response)),
     
    };

    OrderServiceMock={
    getProductByPercent: jest.fn(() => of(response)),}
    fixture = new KpiComponent(ReservationServiceMock,OrderServiceMock);
    fixture.ngOnInit();
  });
  describe('Test: ngOnInit', () => {
    it('should call getAllIncomesByDate on init', () => {
      expect(ReservationServiceMock. getAllReservationsByDateAsc).toBeDefined();
    });
    it('should call getAllIncomesByDate on init', () => {
      expect(ReservationServiceMock.getAllReservationsByMonth).toBeDefined();
    });
    it('should call getAllIncomesByDate on init', () => {
      expect(ReservationServiceMock.getAllReservationsByYear).toBeDefined();
    });
   
  });


  it('should set xAxe and yAxe with data from getAllIncomesByDate', () => {
    fixture.xAxe.push('2022-01-01');
    fixture.yAxe.push(22);

    expect(fixture.xAxe).toEqual(['2022-01-01']);
    expect(fixture.yAxe).toEqual([22]);
  });

  it('should set xAxe and yAxe with data from  getAllIncomesByMonth', () => {
    fixture.xAxe2.push('2022-01-01');
    fixture.yAxe2.push(22);

    expect(fixture.xAxe2).toEqual(['2022-01-01']);
    expect(fixture.yAxe2).toEqual([22]);
  });


  it('should set xAxe and yAxe with data from  getAllIncomesByMonth', () => {
    fixture.xAxe3.push('2022-01-01');
    fixture.yAxe3.push(22);

    expect(fixture.xAxe3).toEqual(['2022-01-01']);
    expect(fixture.yAxe3).toEqual([22]);
  });

  it('should set xAxe and yAxe with data from  getAllIncomesByMonth', () => {
    fixture.xAxeD.push('2022-01-01');
    fixture.yAxeD.push(22);

    expect(fixture.xAxeD).toEqual(['2022-01-01']);
    expect(fixture.yAxeD).toEqual([22]);
  });


  it('should update chart when xAxe and yAxe change', () => {
    fixture.xAxe = ['2022-01-01'];
    fixture.yAxe = [22];

   expect(fixture.chartOptions.xaxis.categories).toEqual(['2022-01-01']);
   expect(fixture.chartOptions.series[0].data).toEqual([22]);
  });
});
