import {ShowInteractionComponent} from './show-interaction.component';
import {of} from "rxjs";

describe('ShowInteractionComponent', () => {
  let fixture: ShowInteractionComponent;
  let interactionServiceMock: any;
  let routerMock: any
  let reclamations: any;
  let suggestions: any;
  let notes: any;

  beforeEach(() => {
    interactionServiceMock = {
      showReclamations: jest.fn(() => of(reclamations)),
      showSuggestions: jest.fn(() => of(suggestions)),
      showNotes: jest.fn(() => of(notes)),
      deleteeInteraction: jest.fn(() => of(suggestions)),
      answerToSuggesion: jest.fn(() => of())
    }
    routerMock = {
      navigate: jest.fn()
    }
    fixture = new ShowInteractionComponent(
      interactionServiceMock,
      routerMock
    );
    fixture.ngOnInit()
  });

  describe('Test: ngOnInit', () => {
    it('should call functions', () => {
      let spyInstanceGetAllRec = jest.spyOn(fixture, 'showNotes');
      expect(spyInstanceGetAllRec).toBeDefined();
    });
  });

  describe('Test: getAllReclamations', () => {
    it('should call showReclamations', () => {
      fixture.getAllReclamations();
      expect(fixture.reclamations).toEqual(reclamations);
    });
  });

  describe('Test: deleteInterction', () => {
    it('should delete ineraction', () => {
      jest.spyOn(fixture, 'getAllReclamations');
      fixture.deleteInterction(7);
      expect(fixture.getAllReclamations).toHaveBeenCalled();
    });
  });

  describe('Test: answerClient', () => {
    it('should navigate to "\'admin/emailresponse\'"', () => {
      fixture.answerClient(7);
      expect(routerMock.navigate).toHaveBeenCalledWith(['admin/emailresponse', 7]);
    });
  });

  describe('Test: answerToSuggesion', () => {
    it('should navigate to "\'admin/emailresponse\'"', () => {
      fixture.answerToSuggesion(7);
      expect(interactionServiceMock.answerToSuggesion).toHaveBeenCalledWith(7);
    });
  });

});
