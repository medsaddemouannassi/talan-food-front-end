import {AllOrdersComponent} from './all-orders.component';
import {of} from "rxjs";

describe('AllOrdersComponent', () => {
  let fixture: AllOrdersComponent;
  let orderServiceMock: any;
  let activaterouterMock: any;
  let routerMock: any;
  let authentificationserviceMock: any
  let data: any

  beforeEach(() => {
    orderServiceMock = {
      getOrdresByReservationId: jest.fn(() => of(data))
    }
    fixture = new AllOrdersComponent(
      orderServiceMock,
      activaterouterMock,
      routerMock,
      authentificationserviceMock
    )
    fixture.ngOnInit()
  });

  describe('Test: ngOnInit', () => {
    it('should get Ordres By Reservation Id', () => {
      expect(orderServiceMock.getOrdresByReservationId).toBeCalled()
      expect(orderServiceMock.orders).toEqual(data)
    });
  });
});
