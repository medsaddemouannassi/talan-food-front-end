import {ShowAllReservationsComponent} from './show-all-reservations.component';
import {of} from "rxjs";
import {FormBuilder} from "@angular/forms";

describe('ShowAllReservationsComponent', () => {
  let fixture: ShowAllReservationsComponent;
  let formBuilderMock: any;
  let datePipeMock: any;
  let reservationServiceMock: any;
  let orderserviceMock: any;
  let routerMock: any;
  let reservations: any;

  beforeEach(() => {
    datePipeMock = {
      transform: jest.fn()
    }
    reservations = [
      {
        "id": 122,
        "price": 11,
        "user": {
          "id": 28,
          "firstName": "user",
          "lastName": "user",
          "email": "user@talan.com",
          "password": "$2a$10$cBjf7x2r44Xu9dJVg6U.ZuRRSszyEOVq6wMzJmQL1lJpy9/AeC.NG",
          "phone": "22222222",
          "role": {
            "id": 2,
            "name": "USER"
          }
        },
        "date": datePipeMock.transform(new Date(), 'yyyy-MM-dd'),
        "confirmed": false
      },
      {
        "id": 127,
        "price": 17,
        "user": {
          "id": 27,
          "firstName": "user",
          "lastName": "user",
          "email": "user@talan.com",
          "password": "$2a$10$cBjf7x2r44Xu9dJVg6U.ZuRRSszyEOVq6wMzJmQL1lJpy9/AeC.NG",
          "phone": "22222222",
          "role": {
            "id": 2,
            "name": "USER"
          }
        },
        "date": datePipeMock.transform(new Date(), 'yyyy-MM-dd'),
        "confirmed": false
      }
    ]
    formBuilderMock = new FormBuilder()
    reservationServiceMock = {
      getAllReservationByDate: jest.fn(() => of()),
      getAllReservations: jest.fn(() => of(reservations))
    }
    fixture = new ShowAllReservationsComponent(
      formBuilderMock,
      datePipeMock,
      reservationServiceMock,
      orderserviceMock,
      routerMock
    )
  });

  afterEach(() => {
    jest.clearAllMocks()
  })

  describe('Test: ngOnInit', () => {
    it('should get reservations when reservations exists by date', () => {
      jest.spyOn(reservationServiceMock, "getAllReservationByDate").mockReturnValue(of(reservations));
      fixture.ngOnInit()
      expect(reservationServiceMock.getAllReservationByDate).toBeCalled();
    });
    it('should get all reservations', () => {
      jest.spyOn(reservationServiceMock, "getAllReservationByDate").mockReturnValueOnce(of([])).mockReturnValue(of(reservations));
      fixture.ngOnInit()
      expect(reservationServiceMock.getAllReservations).toBeCalled();
      expect(reservationServiceMock.getAllReservationByDate).toBeCalledTimes(2);
    });
  });

  describe('Test: getAllreservationByDate', () => {
    it('should get all reservation by date', () => {
      fixture.myDate = datePipeMock.transform(new Date(), 'yyyy-MM-dd')
      jest.spyOn(reservationServiceMock, "getAllReservationByDate").mockReturnValue(of(reservations));
      fixture.ngOnInit()
      fixture.getAllreservationByDate()
      expect(reservationServiceMock.getAllReservationByDate).toBeCalled();
    });
  });

  describe('Test: fetchDateSelected', () => {
    it('should get all reservation by date', () => {
      fixture.myDate = datePipeMock.transform(new Date(), 'yyyy-MM-dd')
      jest.spyOn(reservationServiceMock, "getAllReservationByDate").mockReturnValue(of(reservations));
      let spyInstance = jest.spyOn(fixture, "getAllreservationByDate");
      fixture.ngOnInit()
      fixture.fetchDateSelected()
      expect(spyInstance).toBeCalled();
    });
  });
});
