import {Component, OnInit} from '@angular/core';
import {ReservationService} from 'src/app/services/reservation.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {OrdersService} from 'src/app/services/orders.service';
import {Router} from '@angular/router';
import {DatePipe} from '@angular/common';


@Component({
  selector: 'app-show-all-reservations',
  templateUrl: './show-all-reservations.component.html',
  styleUrls: ['./show-all-reservations.component.css'],
  providers: [DatePipe]

})


export class ShowAllReservationsComponent implements OnInit {
  
  constructor(private formBuilder: FormBuilder, private datePipe: DatePipe, private reservationService: ReservationService, private orderservice: OrdersService, private router: Router) {
  }

  selectDate !: FormGroup;
  allreservation!: any;
  myDate!: any;
  reservation: any;
  dateForm: any;
  n!:number;

  ngOnInit(): void {
    this.myDate = this.datePipe.transform(new Date(), 'yyyy-MM-dd');
    // this.myDate = this.allreservation.some((reservation: { date: any; }) => reservation.date == this.myDate) ? this.allreservation.find((object: { date: any; }) => object.date == this.myDate).date : this.allreservation.find((object: { date: any; }) => object.date > this.myDate).date;
    this.reservationService.getAllReservationByDate(this.myDate).subscribe(data => {
      if (data.length > 0) {
        this.allreservation = data;
      } else {
        this.reservationService.getAllReservations().subscribe(data => {
          data.sort((objA: { date: any; }, objB: { date: any; }) => new Date(objA.date).getTime() - new Date(objB.date).getTime());
          this.myDate = data.some((reservation: { date: any; }) => reservation.date == this.myDate) ? data.find((object: { date: any; }) => object.date == this.myDate).date : data.find((object: { date: any; }) => object.date > this.myDate).date;
          this.reservationService.getAllReservationByDate(this.myDate).subscribe(data => {
            this.allreservation = data;
            this.getReser(this.myDate)
                  this.n=data.length;

          })

        })
      }
    });


    this.selectDate = this.formBuilder.group({
        dateSelected: [this.myDate, Validators.required],
      }
    );

    // this.fetchDateSelected();


  }

  getReser(date: any) {
    this.dateForm = date;
  }

  getAllreservationByDate() {
    this.reservationService.getAllReservationByDate(this.selectDate.value.dateSelected).subscribe(data => {
      this.allreservation = data;
      this.n=data.length;

    });
  }


  fetchDateSelected() {
    this.getAllreservationByDate();
    
  }


}
