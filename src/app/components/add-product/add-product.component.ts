import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { ProductService } from 'src/app/services/product.service';
import { UserService } from 'src/app/services/user.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-add-product',
  templateUrl: './add-product.component.html',
  styleUrls: ['./add-product.component.css'],
})
export class AddProductComponent implements OnInit {
  addProductForm!: FormGroup;
  product: any;
  id!: number;
  selectedFile!: File;
  addId: any;
  origin: any;
  imagePreview: any;

  constructor(
    private fb: FormBuilder,
    private productService: ProductService,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.origin = +this.activatedRoute.snapshot.params['origin'];
    this.addId = +this.activatedRoute.snapshot.params['id'];
    this.addProductForm = this.fb.group({
      name: ['', [Validators.required]],
      description: ['', [Validators.required]],
      quantity: ['', [Validators.required]],
      price: ['', [Validators.required]],
      displayed: ['true'],
      category: [`${this.addId}`],
    });
  }

  onFileSelected(any: any) {
    this.selectedFile = any.target.files[0];
    const reader = new FileReader();
    reader.onload = () => {
      this.imagePreview = reader.result as string;
    };
    reader.readAsDataURL(this.selectedFile);
  }

  addProduct() {
    const formData = new FormData();
    formData.append('profileImage', this.selectedFile);
    formData.append('name', this.addProductForm.value.name);
    formData.append('description', this.addProductForm.value.description);
    formData.append('quantity', this.addProductForm.value.quantity);
    formData.append('price', this.addProductForm.value.price);
    formData.append('displayed', this.addProductForm.value.displayed);
    formData.append('category', this.addProductForm.value.category);

    this.productService.addProduct(formData).subscribe((data) => {
      this.product = data;
      const Toast = Swal.mixin({
        toast: true,

        position: 'top-end',

        showConfirmButton: false,

        timer: 2000,

        timerProgressBar: true,

        didOpen: (toast) => {
          toast.addEventListener('mouseenter', Swal.stopTimer);

          toast.addEventListener('mouseleave', Swal.resumeTimer);
        },
      });

      Toast.fire({
        icon: 'success',

        title: ' Produit ajouté avec succès',
      }).then(() => {
        if (this.origin == 1) {
          this.router.navigateByUrl('admin/products');
        } else {
          this.router.navigateByUrl('admin/add-menu');
        }
      });
    });
  }

  return() {
    if (this.origin == 1) {
      this.router.navigateByUrl('admin/products');
    } else {
      this.router.navigateByUrl('admin/add-menu');
    }
  }
}
