import {EmailresponseComponent} from './emailresponse.component';
import {FormBuilder} from "@angular/forms";
import {of} from "rxjs";

describe('EmailresponseComponent', () => {
  let component: EmailresponseComponent;
  let routerMock: any;
  let formBuilderMock: any;
  let interactionServiceMock: any;
  let activatedRouteMock: any;

  beforeEach(() => {
    formBuilderMock = new FormBuilder()
    routerMock = {
      navigateByUrl: jest.fn()
    }
    activatedRouteMock = {
      snapshot: {
        params: jest.fn()
      }
    }
    interactionServiceMock = {
      answerClient: jest.fn(() => of())
    }
    component = new EmailresponseComponent(
      routerMock,
      formBuilderMock,
      interactionServiceMock,
      activatedRouteMock
    )
    component.ngOnInit()
  });

  describe('Test: ngOnInit', () => {
    it('should initialize emailForm', () => {
      expect(component.emailForm.value.message).toEqual("");
    });
  });

  describe('Test: Onreturn', () => {
    it('should navigate to show interaction', () => {
      component.Onreturn()
      expect(routerMock.navigateByUrl).toBeCalledWith("admin/showIntercation");
    });
  });

  describe('Test: sendEmail', () => {
    it('should send email', () => {
      let spyInstance = jest.spyOn(component, "Onreturn");
      component.sendEmail()
      expect(interactionServiceMock.answerClient).toBeCalled();
    });
  });

  describe('Test: returnToInteractionsList', () => {
    it('should navigate to show interaction', () => {
      component.returnToInteractionsList()
      expect(routerMock.navigateByUrl).toBeCalledWith("admin/showIntercation");
    });
  });
});
