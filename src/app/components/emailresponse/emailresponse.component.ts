import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {InteractionService} from 'src/app/services/interaction.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-emailresponse',
  templateUrl: './emailresponse.component.html',
  styleUrls: ['./emailresponse.component.css']
})
export class EmailresponseComponent implements OnInit {

  constructor(private router: Router, private formBuilder: FormBuilder, private interactionService: InteractionService, private activatedRouter: ActivatedRoute) {
  }

  emailForm !: FormGroup;
  idReclamation!: number;

  ngOnInit(): void {

    this.idReclamation = this.activatedRouter.snapshot.params['id'];

    this.emailForm = this.formBuilder.group(
      {
        message: ['', [Validators.required]]
      }
    );

    this.idReclamation = this.activatedRouter.snapshot.params['id'];

  }


  Onreturn() {
    this.router.navigateByUrl("admin/showIntercation");
  }


  sendEmail() {
    this.interactionService.answerClient(this.idReclamation, this.emailForm.value.message).subscribe();
    const Toast = Swal.mixin({
      toast: true,
      position: 'top-end',
      showConfirmButton: false,
      timer: 3000,
      timerProgressBar: true,
      didOpen: (toast) => {
        toast.addEventListener('mouseenter', Swal.stopTimer)
        toast.addEventListener('mouseleave', Swal.resumeTimer)
      }
    })

    Toast.fire({
      icon: 'success',
      title: ' Reponse envoyée avec succès'
    }).then(() => {
      this.Onreturn();
    })
  }

  returnToInteractionsList() {
    this.router.navigateByUrl("admin/showIntercation")

  }


}
