import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {ProductService} from 'src/app/services/product.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-edit-product',
  templateUrl: './edit-product.component.html',
  styleUrls: ['./edit-product.component.css']
})
export class EditProductComponent implements OnInit {
  product: any;
  productId: any;
  editProductForm!: FormGroup;
  selectedFile!: File
  imagePreview: any;


  constructor(private modalService: NgbModal, private activatedRoute: ActivatedRoute,
              private productService: ProductService, private router: Router, private fb: FormBuilder,) {
  }

  ngOnInit(): void {
    this.productId = +this.activatedRoute.snapshot.params['id'];
    this.productService.getProduct(this.productId).subscribe(data => {
      this.product = data;

      this.editProductForm = this.fb.group({

        name: ['', [Validators.required]],
        description: ['', [Validators.required]],
        quantity: ['', [Validators.required]],
        price: ['', [Validators.required]],
        displayed: ['true'],
        category: [`${this.product.category.id}`],

      })
      this.editProductForm.patchValue({

        name: this.product.name,
        description: this.product.description,
        quantity: this.product.quantity,
        price: this.product.price

      })

    });


  }

  onFileSelected(any: any) {
    this.selectedFile = any.target.files[0];
    const reader = new FileReader();
    reader.onload = () => {
      this.imagePreview = reader.result as string
    };
    reader.readAsDataURL(this.selectedFile);
  }


  updateProduct() {

    const formData = new FormData();
    formData.append('id', this.product.id);
    formData.append('profileImage', this.selectedFile);
    formData.append('name', this.editProductForm.value.name);
    formData.append('description', this.editProductForm.value.description);
    formData.append('quantity', this.editProductForm.value.quantity);
    formData.append('price', this.editProductForm.value.price);
    formData.append('displayed', this.editProductForm.value.displayed);
    formData.append('category', this.editProductForm.value.category);

    this.productService.updateProduct(formData).subscribe(
      data => {
        this.product = data;

        const Toast = Swal.mixin({

          toast: true,

          position: 'top-end',

          showConfirmButton: false,

          timer: 2000,

          timerProgressBar: true,

          didOpen: (toast) => {

            toast.addEventListener('mouseenter', Swal.stopTimer)

            toast.addEventListener('mouseleave', Swal.resumeTimer)

          }

        })


        Toast.fire({

          icon: 'success',

          title: ' Modification en cours'

        }).then(() => {

          this.router.navigateByUrl('admin/products');

        })


      }
    );


  }

  deleteProduct(id: any) {
    if (this.product.category.id == 1 || this.product.category.id == 2 || this.product.category.id == 3 || this.product.category.id == 4) {
      Swal.fire({
        title: '<small>Tous les menus contenant ce produit seront supprimés!</small>',
        width: 777,
        showDenyButton: false,
        showCancelButton: true,
        confirmButtonColor: '#17a2b8',
        cancelButtonText: 'Annuler',
        confirmButtonText: 'Oui',
        denyButtonText: `Non`,
      }).then((result) => {
        /* Read more about isConfirmed, isDenied below */
        if (result.isConfirmed) {
          this.productService.deleteProduct(id).subscribe();
          const Toast = Swal.mixin({
            toast: true,
            position: 'top-end',
            showConfirmButton: false,
            timer: 3500,
            timerProgressBar: true,
            didOpen: (toast) => {
              toast.addEventListener('mouseenter', Swal.stopTimer)
              toast.addEventListener('mouseleave', Swal.resumeTimer)
            }
          })
          Toast.fire({
            icon: 'success',
            title: 'Le produit a été supprimé'
          }).then(() => {
            this.router.navigateByUrl('admin/products');
          })
        } else if (result.isDenied) {
          this.router.navigateByUrl('admin/products');
        }
      })
    } else {
      Swal.fire({
        title: '<small>Voulez-vous vraiment supprimer ce produit ?</small>',      
        width: 700,
        showDenyButton: false,
        showCancelButton: true,
        confirmButtonColor: '#17a2b8',
        cancelButtonText: 'Annuler',
        confirmButtonText: 'Oui',
        denyButtonText: `Non`,
      }).then((result) => {
        /* Read more about isConfirmed, isDenied below */
        if (result.isConfirmed) {
          this.productService.deleteProduct(id).subscribe();
          const Toast = Swal.mixin({
            toast: true,
            position: 'top-end',
            showConfirmButton: false,
            timer: 3500,
            timerProgressBar: true,
            didOpen: (toast) => {
              toast.addEventListener('mouseenter', Swal.stopTimer)
              toast.addEventListener('mouseleave', Swal.resumeTimer)
            }
          })
          Toast.fire({
            icon: 'success',
            title: 'Le produit a été supprimé'
          }).then(() => {
            this.router.navigateByUrl('admin/products');
          })
        } else if (result.isDenied) {
          this.router.navigateByUrl('admin/products');
        }
      })
    }
  }
}
