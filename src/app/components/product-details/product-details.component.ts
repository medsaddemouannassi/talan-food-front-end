import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {AuthenticationService} from 'src/app/services/authentication.service';
import {ProductService} from 'src/app/services/product.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-product-details',
  templateUrl: './product-details.component.html',
  styleUrls: ['./product-details.component.css']
})
export class ProductDetailsComponent implements OnInit {
  isUserLoggedIn!: boolean;
  product: any;
  productId: any;
  rating:any;
  userId: any
  newRating!: any

  rateProductForm!: FormGroup;


  constructor(private modalService: NgbModal, public activatedRoute: ActivatedRoute,
              private productService: ProductService, private router: Router, private authSerivce: AuthenticationService, private formBuilder: FormBuilder) {
  }

  ngOnInit(): void {
    this.rateProductForm = this.formBuilder.group({
        value: ['', [Validators.required]],
      }
    )
    this.isUserLoggedIn = this.authSerivce.isUserLoggedIn();
    this.userId = this.isUserLoggedIn ? this.authSerivce.getUserFromLocalCache().id : null;
    this.productId = +this.activatedRoute.snapshot.params['id'];
    this.productService.getProduct(this.productId).subscribe(
      data => {
        this.product = data;
      }
    );

    this.productService.getRating(this.productId).subscribe(
      data => {
        this.rating = data;
        console.log(this.rating)
      });

  }

  imageSrc = 'assets/images/product/1.jpg';

  sizeClass = "";


  onClick(imagename: any) {
    this.imageSrc = "assets/images/product/" + imagename;
  }

  open(content: any) {
    this.modalService.open(content);
  }

  toggleSizeClass(size: any) {
    this.sizeClass = size;
  }


  getRating(id: any) {
    this.productService.getRating(id).subscribe(data => {
      this.rating = data
    })
  }

  rateProduct() {

    this.newRating = {
      value: this.rateProductForm.value.value,
      user: {id: this.userId},
      product: {id: this.productId}
    }
    this.productService.rateProduct(this.newRating).subscribe(() => {
      this.refreshRating()
    })
  }

  refreshRating() {
    this.productService.getRating(this.productId).subscribe(data => {
      this.rating = data;
      const Toast = Swal.mixin({

        toast: true,

        position: 'top-end',

        showConfirmButton: false,

        timer: 2000,

        timerProgressBar: true,

        didOpen: (toast) => {

          toast.addEventListener('mouseenter', Swal.stopTimer)

          toast.addEventListener('mouseleave', Swal.resumeTimer)

        }

      })


      Toast.fire({

        icon: 'success',

        title: ' Merci pour votre évaluation'

      }).then(() => {


        this.router.navigateByUrl(`/products/${this.productId}`);


      })
    })

  }
}
