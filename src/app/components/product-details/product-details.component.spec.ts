
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

import {  of } from 'rxjs';


import { ProductDetailsComponent } from './product-details.component';

describe('ProductDetailsComponent', () => {
  
  
  let component: ProductDetailsComponent;
  let  modalService: any
  let productServiceMock: any;
  let authServiceMock: any;
  let activatedRoute: any
  let router: any;
  let formBuilder: FormBuilder
  let form: any
  let user = {
    email: "user@talan.com",
    firstName: "user",
    id: 17,
    lastName: "user",
    password: null,
    phone: "22222222",
    role: {id: 2, name: "USER"}
  }

  beforeEach(async () => {
  

    activatedRoute={
      snapshot:{
        params:{
          'id':5
        }
      }
    }
    router=jest.fn()
    modalService=jest.fn()
    productServiceMock={
      getRating: jest.fn(()=>of(4)),
      getProduct:jest.fn(()=>of({"id": 1,
      "name": "koseksi",
      "description": "bnine",
      "quantity": 9,
      "price": 14.5,
      "image": "image",
      "displayed": true,
      "category": {
        "id": 1,
        "name": "pp"
      }
    })),
      rateProduct:jest.fn(()=>of(4))
    }
    authServiceMock={
      getUserFromLocalCache: jest.fn(()=>user),
      isUserLoggedIn: jest.fn(()=>true),
    }
    formBuilder=new FormBuilder(),

    component= new ProductDetailsComponent(modalService,activatedRoute,
      productServiceMock, router, authServiceMock , formBuilder);
     
     component.productId=5
    

    
  });

  it('should created',()=>{
    expect(component).toBeTruthy
  })

  it('should get rating',()=>{
    component.getRating(8)
    expect(productServiceMock.getRating).toHaveBeenCalled
    expect(component.rating).toEqual(4)
   
  })

  it('should refresh rating',()=>{
    form ={
      value:'5'
     }
     component.rateProductForm=form
    
    component.rateProduct()
    expect(productServiceMock.getRating).toHaveBeenCalled
    expect(component.rating).toEqual(4)
   
  })

  it('should rate product',()=>{
    component.refreshRating()
    expect(productServiceMock.rateProduct).toHaveBeenCalled
    expect(component.refreshRating).toHaveBeenCalled
   
  })

  it('should rate product',()=>{
    component.onClick('a')
    expect(component.imageSrc).toEqual("assets/images/product/a")
    
   
  })
  it('should init',()=>{
   form ={
    value:''
   }
    component.ngOnInit()
    expect(component.rateProductForm.value).toEqual(form)
    expect(authServiceMock.isUserLoggedIn).toHaveBeenCalled
    expect(authServiceMock.getUserFromLocalCache).toHaveBeenCalled
    expect(component.productId).toEqual(5)
    expect(productServiceMock.getProduct).toHaveBeenCalled
   
  })

  it('add image source',()=>{
    component.onClick("imageName")
    expect(component.imageSrc).toEqual("assets/images/product/imageName")

    
   })



});
