import {OrderProductCardComponent} from './order-product-card.component';
import {of} from "rxjs";

describe('OrderProductCardComponent', () => {
  let fixture: OrderProductCardComponent;
  let authenticationServiceMock: any;
  let activatedRouteMock: any;
  let routerMock: any;
  let productServiceMock: any;

  beforeEach(() => {
    authenticationServiceMock = {
      isUserLoggedIn: jest.fn()
    }
    productServiceMock = {
      getRating: jest.fn(() => of(7))
    }
    routerMock = {
      navigateByUrl: jest.fn()
    }
    fixture = new OrderProductCardComponent(
      authenticationServiceMock,
      activatedRouteMock,
      routerMock,
      productServiceMock
    )
    fixture.product = {
      "id": 1,
      "name": "koseksi",
      "description": "bnine",
      "quantity": 9,
      "price": 14.5,
      "image": "image",
      "displayed": true,
      "category": {
        "id": 1,
        "name": "pp"
      }
    }
    fixture.ngOnInit()
  });

  describe('Test: ngOnInit', () => {
    it('should initialize attributes', () => {
      expect(fixture.rating).toEqual(7);
    });
  });

  describe('Test: showDetails', () => {
    it('should navigate to product', () => {
      fixture.showDetails(7)
      expect(routerMock.navigateByUrl).toBeCalledWith(`products/7`);
    });
  });

  describe('Test: addQuantity', () => {
    it('should add quantity', () => {
      fixture.quantity = 6
      fixture.addQuantity()
      expect(fixture.quantity).toEqual(7);
    });
    it('should not add quantity', () => {
      fixture.quantity = 11
      fixture.addQuantity()
      expect(fixture.quantity).toEqual(11);
    });
  });

  describe('Test: removeQuantity', () => {
    it('should remove quantity', () => {
      fixture.quantity = 30
      fixture.removeQuantity()
      expect(fixture.quantity).toEqual(29);
    });
    it('should not remove quantity', () => {
      fixture.quantity = 0
      fixture.removeQuantity()
      expect(fixture.quantity).toEqual(0);
    });
  });

  describe('Test: getRating', () => {
    it('should remove quantity', () => {
      fixture.getRating(7)
      expect(productServiceMock.getRating).toBeCalledWith(7);
    });
  });
});
