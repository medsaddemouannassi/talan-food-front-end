import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { ProductService } from 'src/app/services/product.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, FormGroup } from '@angular/forms';
import Swal from 'sweetalert2';
import { OrdersService } from 'src/app/services/orders.service';
import { DatePipe } from '@angular/common';
@Component({
  selector: 'app-order-product-card',
  templateUrl: './order-product-card.component.html',
  styleUrls: ['./order-product-card.component.css'],
  providers: [DatePipe],
})
export class OrderProductCardComponent implements OnInit {
  isUserLoggedIn!: boolean;
  rateProductForm!: FormGroup;
  newRating!: any;
  userId: any;
  myDate: any;
  display: boolean = false;

  list: any = [];
  date: any;
  ProductStatus = false;
  ratingUser: any = [];
  @Input() product!: any;
  @Output() quantityEvent = new EventEmitter<any>();
  rating: any;
  order = {
    id: '',
    quantity: '',
    totalPrice: 0,
    reste: 0,
  };
  quantity: any = 0;

  constructor(
    private modalService: NgbModal,
    private fb: FormBuilder,
    private datePipe: DatePipe,
    private authenticationService: AuthenticationService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private productService: ProductService,
    private orderService: OrdersService
  ) {}

  ngOnInit(): void {
    this.myDate = this.datePipe.transform(new Date(), 'yyyy-MM-dd');
    this.rateProductForm = this.fb.group({
      value: [],
      product: [],
    });
    this.isUserLoggedIn = this.authenticationService.isUserLoggedIn();

    this.userId = this.isUserLoggedIn
      ? this.authenticationService.getUserFromLocalCache().id
      : null;
    console.log(this.userId);
    this.productService.getRating(this.product.id).subscribe((data) => {
      this.rating = data;
    });

    this.productService
      .rateProductByUser(this.userId, this.product.id)
      .subscribe((data) => {
        this.ratingUser = data;
        console.log(data);
      });

    this.order.id = this.product.id;
    this.orderService.getOrdresByUserId(this.userId).subscribe((response) => {
      response.forEach((element: any) => {
        if (
          element.product.id == this.product.id &&
          element.reservation.date <= this.myDate
        ) {
          this.display = true;
          if (this.ratingUser.length === 0) {
            const Toast = Swal.mixin({
              toast: true,

              position: 'top-end',

              showConfirmButton: false,

              timer: 2000,

              timerProgressBar: true,

              didOpen: (toast) => {
                toast.addEventListener('mouseenter', Swal.stopTimer);

                toast.addEventListener('mouseleave', Swal.resumeTimer);
              },
            });

            Toast.fire({
              icon: 'info',

              title: ` Merci de donner une note  sur le produit <b>${element.product.name}<b>`,
            }).then(() => {
              this.router.navigateByUrl(`/products`);
            });
          }
        }
      });
    });
  }

  showDetails(id: number) {
    this.router.navigateByUrl(`products/${id}`);
  }

  addQuantity() {
    if (this.quantity < this.product.quantity) {
      this.quantity++;
      this.order.quantity = this.quantity;
      this.order.totalPrice = this.quantity * this.product.price;
      this.order.reste = this.product.quantity - this.quantity;
      this.quantityEvent.emit(this.order);
    }
  }

  removeQuantity() {
    if (this.quantity > 0) {
      this.quantity--;
      this.order.quantity = this.quantity;
      this.order.reste = this.product.quantity - this.quantity;
      this.order.totalPrice = this.quantity * this.product.price;
      this.quantityEvent.emit(this.order);
    }
  }

  getRating(id: any) {
    this.productService.getRating(id).subscribe((data) => {
      this.rating = data;
      console.log(this.rating + 'hello');
    });
  }

  rateProduct() {
    console.log(this.rateProductForm.value);

    this.newRating = {
      value: this.rateProductForm.value.value,
      user: { id: this.userId },
      product: { id: this.rateProductForm.value.product },
    };

    this.productService.rateProduct(this.newRating).subscribe(() => {
      this.refreshRating();
      console.log(this.refreshRating());
    });
  }

  refreshRating() {
    this.rating = {};
    this.productService
      .getRating(this.rateProductForm.value.product)
      .subscribe((data) => {
        this.rating = data;
        const Toast = Swal.mixin({
          toast: true,

          position: 'top-end',

          showConfirmButton: false,

          timer: 2000,

          timerProgressBar: true,

          didOpen: (toast) => {
            toast.addEventListener('mouseenter', Swal.stopTimer);

            toast.addEventListener('mouseleave', Swal.resumeTimer);
          },
        });

        Toast.fire({
          icon: 'success',

          title: ' Merci pour votre évaluation',
        }).then(() => {
          this.router.navigateByUrl(`/products`);
        });
      });
  }
  open(content: any, product: any) {
    this.rateProductForm.patchValue({
      product: product.id,
    });
    this.modalService.open(content);
  }
}
