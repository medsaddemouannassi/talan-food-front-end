import { of } from 'rxjs';
import { MenuItemComponent } from './menu-item.component';

describe('MenuItemComponent', () => {
  let fixture: MenuItemComponent;
  let authenticationServiceMock: any;
  let subscriptionServiceMock: any;
  let product: any;

  beforeEach(() => {
    let item = { id: 4, quantity: 14, price: '100' };
    let user = { id: 3 };
    authenticationServiceMock = {
      isUserLoggedIn: jest.fn(() => true),
      getUserFromLocalCache: jest.fn(() => user),
    };
    subscriptionServiceMock = {
      getSubscriptionStatus: jest.fn(() => of(true)),
    };
    fixture = new MenuItemComponent(
      authenticationServiceMock,
      subscriptionServiceMock
    );
    fixture.ngOnInit();
  });

  describe('Test: ngOnInit', () => {
    it('should initialize attributes', () => {
      expect(fixture.isUserLoggedIn).toEqual(true);
    });
  });

  describe('Test: onIncrement', () => {
    it('should increment quantity', () => {
      let item = {
        id: 1,
        quantity: 5,
        price: '',
      };
      product = {
        id: '',
        quantity: 0,
        price: '',
      };
      fixture.product = product;
      fixture.item = item;
      fixture.onIncrement(1, 10);
      expect(fixture.quantity).toEqual(1);
    });
    it('should not increment quantity', () => {
      let item = {
        id: 1,
        quantity: 0,
        price: '',
      };
      product = {
        id: '',
        quantity: 0,
        price: '',
      };
      fixture.product = product;
      fixture.item = item;
      fixture.onIncrement(1, 10);
      expect(fixture.quantity).toEqual(0);
    });
  });

  describe('Test: onDecrement', () => {
    it('should decrement quantity', () => {
      fixture.quantity = 10;
      fixture.onDecrement(1, 10);
      expect(fixture.quantity).toEqual(9);
    });
    it('should not decrement quantity', () => {
      fixture.quantity = 0;
      fixture.onDecrement(1, 10);
      expect(fixture.quantity).toEqual(0);
    });
  });
});
