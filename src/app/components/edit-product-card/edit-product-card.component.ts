import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-edit-product-card',
  templateUrl: './edit-product-card.component.html',
  styleUrls: ['./edit-product-card.component.css']
})
export class EditProductCardComponent implements OnInit {

  @Input() product!: any;
  @Output() quantityEvent = new EventEmitter<any>();

  quantity: any = 0;

  constructor(private activatedRoute: ActivatedRoute, private router: Router) {
  }

  ngOnInit(): void {


  }

  editProduct(id: number) {
    this.router.navigateByUrl(`admin/products/${id}`);
  }
}
