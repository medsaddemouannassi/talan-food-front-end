import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditProductCardComponent } from './edit-product-card.component';

describe('EditProductCardComponent', () => {
  let component: EditProductCardComponent;
  let activatedRouteMock: any;
  let routerMock: any;

  beforeEach(() => {
    routerMock = {
      navigateByUrl: jest.fn()
    }
    component = new EditProductCardComponent(
      activatedRouteMock,
      routerMock
    )
    component.ngOnInit()
  });

  describe('Test: editProduct', () => {
    it('should edit product', () => {
      component.editProduct(7)
      expect(routerMock.navigateByUrl).toBeCalledWith(`admin/products/7`)
    });
  });
});
