import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { OrdersService } from 'src/app/services/orders.service';
import { ProductService } from 'src/app/services/product.service';
import { ReservationService } from 'src/app/services/reservation.service';
import { UserService } from 'src/app/services/user.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-order-product',
  templateUrl: './order-product.component.html',
  styleUrls: ['./order-product.component.css'],
})
export class OrderProductComponent implements OnInit {
  // Hamza use those two to create a reservation
  orders: any = [];
  reservationDate: any;
  canOrder: boolean = false;
  today: any;

  permitAddProduct!: boolean;
  order: any;
  date = new Date();

  products: any;
  quantity: any = 0;
  reservation!: any;

  constructor(
    private datepipe: DatePipe,
    private productService: ProductService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private reservationService: ReservationService,
    private authenticationService: AuthenticationService,
    private userService: UserService,
    private ordersService: OrdersService
  ) {}

  ngOnInit(): void {
    this.date = new Date();
    this.today = this.datepipe.transform(this.date, 'yyyy-MM-dd');
    this.permitAddProduct =
      !!this.authenticationService.getRoles()?.includes('USER') ||
      !this.authenticationService.getRoles();

    this.productService.getProducts().subscribe((data) => {
      this.products = data;
    });
  }

  showDetails(id: number) {
    this.router.navigateByUrl(`user/products/${id}`);
  }

  addOrder(order: any) {
    this.orders.map((ord: { id: any }) =>
      ord.id == order.id
        ? this.orders.splice(this.orders.indexOf(ord, 0), 1)
        : false
    );
    this.orders.push(order);
    this.orders.map((ord: { quantity: any }) =>
      ord.quantity == 0
        ? this.orders.splice(this.orders.indexOf(ord, 0), 1)
        : false
    );
  }

  changeReservationDate(any: any) {
    let compareDate = any.target.value;

    if (this.today <= compareDate) {
      this.reservationDate = compareDate;
      this.canOrder = true;

      this.reservation = {
        id: null,
        user: { id: this.authenticationService.getUserFromLocalCache().id },
        date: this.reservationDate,
        price: null,
      };
    } else {
      this.canOrder = false;
    }
  }

  // Hamza this the method you will make to create a reservation
  createReservation() {
    if (this.authenticationService.getUserFromLocalCache() == null) {
      const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000,
        timerProgressBar: true,
        didOpen: (toast) => {
          toast.addEventListener('mouseenter', Swal.stopTimer);
          toast.addEventListener('mouseleave', Swal.resumeTimer);
        },
      });
      Toast.fire({
        icon: 'info',
        title: "Vous devez d'abord vous connecter",
      }).then(() => {
        this.router.navigateByUrl('/page-login');
      });
    } else {
      this.reservationService
        .addReservation(this.reservation)
        .subscribe((data) => {
          this.reservation = data;
          for (const element of this.orders) {
            let order = {
              reservation: { id: this.reservation.id },
              product: { id: element.id },
              quantity: element.quantity,
            };
            this.reservation.price =
              this.reservation.price + element.totalPrice;
            this.productService
              .updateQuantity(element.id, element.reste)
              .subscribe();

            this.ordersService.addOrder(order).subscribe();
          }

          this.reservationService
            .confirmReservation(this.reservation)
            .subscribe();

          const Toast = Swal.mixin({
            toast: true,

            position: 'top-end',

            showConfirmButton: false,

            timer: 2000,

            timerProgressBar: true,

            didOpen: (toast) => {
              toast.addEventListener('mouseenter', Swal.stopTimer);

              toast.addEventListener('mouseleave', Swal.resumeTimer);
            },
          });

          Toast.fire({
            icon: 'success',

            title: ' Réservation confirmée ',
          }).then(() => {
            this.router.navigateByUrl('getReservationByUserId');
          });
        });
    }
  }
}
