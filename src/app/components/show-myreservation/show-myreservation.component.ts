import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {Reservation} from 'src/app/models/Reservation';
import {AuthenticationService} from 'src/app/services/authentication.service';
import {ReservationService} from 'src/app/services/reservation.service';
import Swal from 'sweetalert2';
import {DatePipe} from "@angular/common";
import { compareByFieldSpecs } from '@fullcalendar/angular';


@Component({
  selector: 'app-show-myreservation',
  templateUrl: './show-myreservation.component.html',
  styleUrls: ['./show-myreservation.component.css'],
  providers: [DatePipe]

})

export class ShowMyreservationComponent implements OnInit {
  reservation!: Reservation[];
  resultAnnulation!: any;
  myDate!: any;
  pageNumber!:number;
  pagesTab !: any;
  userId!:number;
  actualPage!:any;

  constructor(private reservationService: ReservationService, private router: Router, private authenticationService: AuthenticationService, private datePipe: DatePipe) {
  }

  ngOnInit(): void {
    this.userId=this.authenticationService.getUserFromLocalCache().id;
    this.getAllResrvationByUserId(this.userId,0);
    this.myDate = this.datePipe.transform(new Date(), 'yyyy-MM-dd');
    console.log(this.pageNumber);
    

  }


  getAllResrvationByUserId(idUser: number , page:number) {
    this.reservationService.getReservationByUserId(idUser,page).subscribe(data => {
      this.actualPage=data.pageable.pageNumber;
      console.log(this.actualPage)
      this.reservation= data.content;
      this.pagesTab = new Array(data.totalPages);
      this.reservation.sort((a,b)=> {
        return new Date(b.date).getTime() - new Date(a.date).getTime() 
      });
    });

  }

  deleteReservationById(id: number) {
    this.reservationService.deleteReservationById(id).subscribe(data => {
      this.resultAnnulation = data;
      this.getAllResrvationByUserId(this.authenticationService.getUserFromLocalCache().id,this.actualPage)
    });
  };


  showReservationDetails(id: number) {
    this.router.navigate([`/ordersreservation`, id]);
  }

  annulerReservation(reservId: number) {
    const Toast = Swal.mixin({

      toast: true,

      position: 'top-end',

      showConfirmButton: false,

      timer: 2000,

      timerProgressBar: true,

      didOpen: (toast) => {

        toast.addEventListener('mouseenter', Swal.stopTimer)

        toast.addEventListener('mouseleave', Swal.resumeTimer)

      }

    })


    Toast.fire({

      icon: 'success',

      title: ' Traitement en cours'

    })
    this.reservationService.deleteReservationById(reservId).subscribe(data => {
      this.resultAnnulation = data;
      if (this.resultAnnulation == true) {
         this.getAllResrvationByUserId(this.authenticationService.getUserFromLocalCache().id,this.actualPage)
         console.log(this.actualPage);

        const Toast = Swal.mixin({
          toast: true,
          position: 'top-end',
          showConfirmButton: false,
          timer: 3000,
          timerProgressBar: true,
          didOpen: (toast) => {
            toast.addEventListener('mouseenter', Swal.stopTimer)
            toast.addEventListener('mouseleave', Swal.resumeTimer)
          }
        })
        Toast.fire({
          icon: 'success',
          title: ' Reservation annulée avec succès'
        })
      } else {

        const Toast = Swal.mixin({
          toast: true,
          position: 'top-end',
          showConfirmButton: false,
          timer: 3000,
          timerProgressBar: true,
          didOpen: (toast) => {
            toast.addEventListener('mouseenter', Swal.stopTimer)
            toast.addEventListener('mouseleave', Swal.resumeTimer)
          }
        })
        Toast.fire({
          icon: 'warning',
          title: ' Vous avez dépassé le délais d\'annulation !'
        })
      }
    });

  }


}
