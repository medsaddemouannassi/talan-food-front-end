import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {ProductService} from 'src/app/services/product.service';

@Component({
  selector: 'app-add-photo',
  templateUrl: './add-photo.component.html',
  styleUrls: ['./add-photo.component.css']
})
export class AddPhotoComponent implements OnInit {
  product: any;
  productId: any;
  addImageForm!: FormGroup;
  selectedFile!: File;


  constructor(private activatedRoute: ActivatedRoute,
              private productService: ProductService, private fb: FormBuilder, private router: Router) {
  }

  ngOnInit(): void {
    this.productId = +this.activatedRoute.snapshot.params['id'];
    this.productService.getProduct(this.productId).subscribe(
      data => {
        this.product = data;
      }
    );
  }

  onFileSelected(any: any) {
    this.selectedFile = any.target.files[0];
  }

  public addImage(): void {
    const formData = new FormData();
    formData.append('product', this.productId);
    formData.append('profileImage', this.selectedFile);

    this.productService.updateProfileImage(formData).subscribe()


  }

}
