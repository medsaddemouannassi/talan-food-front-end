import {AddPhotoComponent} from './add-photo.component';
import {of} from "rxjs";

describe('AddPhotoComponent', () => {
  let fixture: AddPhotoComponent;
  let activatedRouteMock: any;
  let productServiceMock: any;
  let formBuilderMock: any;
  let routerMock: any;
  let product: any

  beforeEach(() => {
    activatedRouteMock = {
      snapshot: {
        params: jest.fn()
      }
    }
    product = {
      "id": 1,
      "name": "koseksi",
      "description": "bnine",
      "quantity": 9,
      "price": 14.5,
      "image": "image",
      "displayed": true,
      "category": {
        "id": 1,
        "name": "pp"
      }
    }
    productServiceMock = {
      getProduct: jest.fn(() => of(product)),
      updateProfileImage: jest.fn(() => of())
    }
    fixture = new AddPhotoComponent(
      activatedRouteMock,
      productServiceMock,
      formBuilderMock,
      routerMock
    )
    fixture.ngOnInit()
  });

  afterEach(() => {
    jest.clearAllMocks()
  })

  describe('Test: ngOnInit', () => {
    it('should get product', () => {
      expect(productServiceMock.getProduct).toBeCalled()
    });
  });

  describe('Test: onFileSelected', () => {
    it('should get file selected', () => {
      let any = {
        target: {
          files: []
        }
      }
      fixture.onFileSelected(any)
      expect(fixture.selectedFile).toEqual(any.target.files[0])
    });
  });

  describe('Test: addImage', () => {
    it('should add image', () => {
      fixture.addImage()
      expect(productServiceMock.updateProfileImage).toBeCalled()
    });
  });
});
