import {MenuComponent} from './menu.component';
import {of} from "rxjs";

describe('MenuComponent', () => {
  let fixture: MenuComponent;
  let menuServiceMock: any;
  let datePipeMock: any;
  let reservationServiceMock: any;
  let authenticationServiceMock: any
  let ordersServiceMock: any;
  let routerMock: any;
  let productServiceMock: any;
  let menus: any;
  let product: any;
  let products: any;
  let reservation: any;

  beforeEach(() => {
    datePipeMock = {
      transform: jest.fn()
    }
    products = [
      {
        "id": 1,
        "name": "koseksi",
        "description": "bnine",
        "quantity": 9,
        "price": 14.5,
        "image": "image",
        "displayed": true,
        "category": {
          "id": 1,
          "name": "pp"
        }
      },
      {
        "id": 11,
        "name": "koseksi",
        "description": "bnine",
        "quantity": 9,
        "price": 14.5,
        "image": "image",
        "displayed": true,
        "category": {
          "id": 2,
          "name": "pp"
        }
      },
      {
        "id": 111,
        "name": "koseksi",
        "description": "bnine",
        "quantity": 9,
        "price": 14.5,
        "image": "image",
        "displayed": true,
        "category": {
          "id": 3,
          "name": "pp"
        }
      },
      {
        "id": 1111,
        "name": "koseksi",
        "description": "bnine",
        "quantity": 9,
        "price": 14.5,
        "image": "image",
        "displayed": true,
        "category": {
          "id": 4,
          "name": "pp"
        }
      }
    ]
    reservation = {
      "id": 121,
      "price": 14.5,
      "user": {
        "id": 28,
        "firstName": "user",
        "lastName": "user",
        "email": "user@talan.com",
        "password": "$2a$10$cBjf7x2r44Xu9dJVg6U.ZuRRSszyEOVq6wMzJmQL1lJpy9/AeC.NG",
        "phone": "22222222",
        "role": {
          "id": 2,
          "name": "USER"
        }
      },
      "date": "2022-07-20",
      "confirmed": false
    };
    menus = [
      {
        "id": 48,
        "date": datePipeMock.transform(new Date(), 'yyyy-MM-dd'),
        "listProducts": products
      }
    ]
    menuServiceMock = {
      getAllMenus: jest.fn(() => of(menus))
    }
    authenticationServiceMock = {
      getRoles: jest.fn(() => ["USER"]),
      getUserFromLocalCache() {
      }
    }
    routerMock = {
      navigateByUrl: jest.fn()
    }
    reservationServiceMock = {
      addReservation: jest.fn(() => of(reservation)),
      confirmReservation: jest.fn()
    }
    ordersServiceMock = {
      addOrder: jest.fn(() => of())
    }
    productServiceMock = {
      getProduct: jest.fn(() => of()),
      updateProductQuantity: jest.fn(() => of())
    }
    fixture = new MenuComponent(
      menuServiceMock,
      datePipeMock,
      reservationServiceMock,
      authenticationServiceMock,
      ordersServiceMock,
      routerMock,
      productServiceMock
    )
    fixture.ngOnInit();
  });

  describe('Test: ngOnInit', () => {
    it('should initialize attributes', () => {
      expect(fixture.timeLimit).toEqual(new Date().getHours() >= 12);
      expect(fixture.myDate).toEqual(datePipeMock.transform(new Date(), 'yyyy-MM-dd'));
    });
  });

  describe('Test: onDateChange', () => {
    it('should change menu on date change', () => {
      fixture.onDateChange(datePipeMock.transform(new Date(), 'yyyy-MM-dd'));
      const spyGetALLMenus = jest.spyOn(menuServiceMock, 'getAllMenus');
      expect(spyGetALLMenus).toBeCalled();
      expect(fixture.salades[0].category.id).toEqual(1)
      expect(fixture.plats[0].category.id).toEqual(2)
      expect(fixture.garnitures[0].category.id).toEqual(3)
      expect(fixture.desserts[0].category.id).toEqual(4)
    });
  });

  describe('Test: addProduct', () => {
    it('should add product', () => {
      product = {
        "id": 1,
        "name": "koseksi",
        "description": "bnine",
        "quantity": 9,
        "price": 14.5,
        "image": "image",
        "displayed": true,
        "category": {
          "id": 1,
          "name": "pp"
        }
      }
      fixture.addProduct(product);
      expect(fixture.permitAddProduct).toBeTruthy()
    });
  });

  describe('Test: confirm', () => {
    it('should not confirm reservation', () => {
      jest.spyOn(authenticationServiceMock, 'getUserFromLocalCache');
      fixture.confirm(datePipeMock.transform(new Date(), 'yyyy-MM-dd'));
      expect(reservationServiceMock.addReservation).not.toBeCalled()
    });
    it('should confirm reservation', () => {
      let user = {
        email: "user@talan.com",
        firstName: "user",
        id: 17,
        lastName: "user",
        password: null,
        phone: "22222222",
        role: {id: 2, name: "USER"}
      }
      fixture.products = products
      jest.spyOn(authenticationServiceMock, 'getUserFromLocalCache').mockReturnValue(user);
      fixture.confirm(datePipeMock.transform(new Date(), 'yyyy-MM-dd'));
      expect(reservationServiceMock.addReservation).toBeCalled()
      let data = jest.spyOn(reservationServiceMock, 'addReservation').mockReturnValue(reservation);
      let order = {
        reservation: data,
        product: products[0],
        quantity: products[0].quantity
      }
      expect(ordersServiceMock.addOrder(order)).toBeDefined()
      expect(ordersServiceMock.addOrder).toBeCalledWith(order)
      expect(productServiceMock.getProduct()).toBeDefined()
      expect(productServiceMock.getProduct).toBeCalled()
      jest.spyOn(productServiceMock, 'getProduct').mockReturnValue(products[0]);
      expect(productServiceMock.getProduct(products[0].id)).toBeDefined()
      expect(productServiceMock.getProduct).toBeCalledWith(1)
      expect(productServiceMock.updateProductQuantity(products[0].id, (products[0].quantity))).toBeDefined()
      expect(productServiceMock.updateProductQuantity).toBeCalledWith(products[0].id, (products[0].quantity))
      expect(reservationServiceMock.confirmReservation).toBeCalledWith(reservation)
    });
  });
});
