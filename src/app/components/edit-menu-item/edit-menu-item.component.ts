import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {ProductService} from 'src/app/services/product.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-edit-menu-item',
  templateUrl: './edit-menu-item.component.html',
  styleUrls: ['./edit-menu-item.component.css']
})
export class EditMenuItemComponent implements OnInit {

  product: any;
  productId: any;
  editProductForm!: FormGroup;
  selectedFile!: File
  imagePreview: any;


  constructor(private modalService: NgbModal, private activatedRoute: ActivatedRoute,
              private productService: ProductService, private router: Router, private fb: FormBuilder,) {
  }

  ngOnInit(): void {
    this.productId = +this.activatedRoute.snapshot.params['id'];
    this.productService.getProduct(this.productId).subscribe(data => {
      this.product = data;
      this.editProductForm = this.fb.group({
        name: ['', [Validators.required]],
        description: ['', [Validators.required]],
        quantity: ['', [Validators.required]],
        price: ['', [Validators.required]],
        displayed: ['true'],
        category: [`${this.product.category.id}`],

      })
      this.editProductForm.patchValue({
        name: this.product.name,
        description: this.product.description,
        quantity: this.product.quantity,
        price: this.product.price
      })
    });
  }

  onFileSelected(any: any) {
    this.selectedFile = any.target.files[0];
    const reader = new FileReader();
    reader.onload = () => {
      this.imagePreview = reader.result as string
    };
    reader.readAsDataURL(this.selectedFile);
  }

  updateProduct() {
    const formData = new FormData();
    formData.append('id', this.product.id);
    formData.append('profileImage', this.selectedFile);
    formData.append('name', this.editProductForm.value.name);
    formData.append('description', this.editProductForm.value.description);
    formData.append('quantity', this.editProductForm.value.quantity);
    formData.append('price', this.editProductForm.value.price);
    formData.append('displayed', this.editProductForm.value.displayed);
    formData.append('category', this.editProductForm.value.category);

    this.productService.updateProduct(formData).subscribe(
      data => {
        this.product = data;
        const Toast = Swal.mixin({
          toast: true,
          position: 'top-end',
          showConfirmButton: false,
          timer: 2000,
          timerProgressBar: true,
          didOpen: (toast) => {
            toast.addEventListener('mouseenter', Swal.stopTimer)
            toast.addEventListener('mouseleave', Swal.resumeTimer)
          }
        })
        Toast.fire({
          icon: 'success',
          title: ' Modification en cours'
        }).then(() => {
          this.router.navigateByUrl('admin/add-menu');
        })
      }
    );

  }


}
