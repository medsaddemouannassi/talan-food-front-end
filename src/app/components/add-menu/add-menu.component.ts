import { Component, OnInit } from '@angular/core';
import {
  FormArray,
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { Router } from '@angular/router';
import { MenuService } from 'src/app/services/menu.service';
import { ProductService } from 'src/app/services/product.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-add-menu',
  templateUrl: './add-menu.component.html',
  styleUrls: ['./add-menu.component.css'],
})
export class AddMenuComponent implements OnInit {
  date = new Date();

  menuForm!: FormGroup;

  entreesDispo: any = [];
  entrees!: any[];

  platsDispo: any = [];
  plats!: any[];

  dessertDispo: any = [];
  desserts!: any[];

  garnitureDispo: any = [];
  garnitures!: any[];

  productsDispo: any = [];

  menu: any = {};
  menuId!: number;

  constructor(
    private fb: FormBuilder,
    private productService: ProductService,
    private menuService: MenuService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.menuForm = this.fb.group({
      dateMenu: ['', [Validators.required]],
      listEntrees: this.fb.array([]),
      listPlats: this.fb.array([]),
      listDesserts: this.fb.array([]),
      listGarnitures: this.fb.array([]),
    });

    this.productService.getProductByCat(1).subscribe((data: any[]) => {
      this.entreesDispo = data;
    });

    this.productService.getProductByCat(2).subscribe((data: any[]) => {
      this.platsDispo = data;
    });

    this.productService.getProductByCat(3).subscribe((data: any[]) => {
      this.garnitureDispo = data;
    });

    this.productService.getProductByCat(4).subscribe((data: any[]) => {
      this.dessertDispo = data;
    });

    this.productService.getProductsForMenu().subscribe((data: any[]) => {
      this.productsDispo = data;
    });
  }

  addMenu() {
    this.menu.date = this.menuForm.value.dateMenu;

    this.entrees = this.menuForm.value.listEntrees;
    this.plats = this.menuForm.value.listPlats;
    this.desserts = this.menuForm.value.listDesserts;
    this.garnitures = this.menuForm.value.listGarnitures;

    let productsId: any = [];
    productsId = productsId.concat(
      this.entrees,
      this.plats,
      this.desserts,
      this.garnitures
    );
    let products: any = [];

    for (let i = 0; i < productsId.length; i++) {
      for (let j = 0; j < this.productsDispo.length; j++) {
        if (productsId[i] == this.productsDispo[j].id) {
          let t: any = [];
          t = this.productsDispo[j];
          products.push(t);
        }
      }
    }

    this.menu.listProducts = products;

    // for (let i = 0; i < productsId.length; i++) {
    //   this.productService.getProduct(productsId[i]).subscribe(
    //     data => {
    //       console.log('here data of get product', data);
    //       products.push(data);
    //       console.log('here products table', products);
    //       this.menu.listProducts = products;
    //     }
    //   )
    // }

    // var dialog = confirm("Ajouter ?");
    // if (dialog) {
    //   this.menuService.addMenu(this.menu).subscribe(
    //     res => {
    //       console.log('Le menu à ajouter', this.menu);
    //       console.log('Resultat ajout', res);
    //       this.router.navigateByUrl('menu');
    //     }
    //   )
    // }
    // else {
    //   this.router.navigateByUrl('admin/add-menu');
    // }

    Swal.fire({
      title: '<small>Voulez-vous ajouter ce menu ?</small>',
      showDenyButton: false,
      showCancelButton: true,
      confirmButtonColor: '#17a2b8',
      cancelButtonText: 'Annuler',
      confirmButtonText: 'Oui',
      denyButtonText: `Non`,
    }).then((result) => {
      /* Read more about isConfirmed, isDenied below */
      if (result.isConfirmed) {
        this.menuService.addMenu(this.menu).subscribe();
        const Toast = Swal.mixin({
          toast: true,
          position: 'top-end',
          showConfirmButton: false,
          timer: 3500,
          timerProgressBar: true,
          didOpen: (toast) => {
            toast.addEventListener('mouseenter', Swal.stopTimer);
            toast.addEventListener('mouseleave', Swal.resumeTimer);
          },
        });
        Toast.fire({
          icon: 'success',
          title: 'Votre menu a été ajouté',
        }).then(() => {
          this.router.navigateByUrl('menu');
        });
      } else if (result.isDenied) {
        this.router.navigateByUrl('admin/add-menu');
      }
    });
  }

  // addMenu() {

  //   this.menu.date = this.menuForm.value.dateMenu;
  //   this.menuService.addMenu(this.menu).subscribe(
  //     data => {
  //       //console.log("data est",data);
  //       this.menuId = data.id;
  //       console.log("menuId", this.menuId);

  //       this.entrees = this.menuForm.value.listEntrees;
  //       this.plats = this.menuForm.value.listPlats;
  //       this.desserts = this.menuForm.value.listDesserts;
  //       this.garnitures = this.menuForm.value.listGarnitures;

  //       let products: any = [];
  //       products = products.concat(this.entrees, this.plats, this.desserts, this.garnitures);

  //       console.log('product id list ', products);

  //       for (let i = 0; i < products.length; i++) {
  //         this.menuService.addProductToMenu(data,this.menuId, products[i]).subscribe(
  //           res => {
  //             console.log(res);
  //           }

  //         );
  //       }
  //     }
  //   )

  //   // const Toast = Swal.mixin({
  //   //   toast: true,
  //   //   position: 'top-end',
  //   //   showConfirmButton: false,
  //   //   timer: 4000,
  //   //   timerProgressBar: true,
  //   //   didOpen: (toast) => {
  //   //     toast.addEventListener('mouseenter', Swal.stopTimer)
  //   //     toast.addEventListener('mouseleave', Swal.resumeTimer)
  //   //   }
  //   // })

  //   // Toast.fire({
  //   //   icon: 'success',
  //   //   title: ' Creation du menu en cours'
  //   // }).then(()=>{
  //   //   this.router.navigateByUrl("/menu")
  //   // })

  // }

  // addMenu() {

  //   this.menu.date = this.menuForm.value.dateMenu;
  //   this.menuService.addMenu(this.menu).subscribe(
  //     data => {
  //       //console.log("data est",data);
  //       this.menuId = data.id;
  //       console.log("menuId", this.menuId);

  //       this.entrees = this.menuForm.value.listEntrees;
  //       for (let i = 0; i < this.entrees.length; i++) {
  //         console.log(this.entrees[i]);
  //         this.menuService.addProductToMenu(this.menuId, this.entrees[i]).subscribe(
  //           res => {
  //             console.log(res);
  //           }
  //         );
  //       }

  //       this.plats = this.menuForm.value.listPlats;
  //       for (let i = 0; i < this.plats.length; i++) {
  //         console.log(this.plats[i]);
  //         this.menuService.addProductToMenu(this.menuId, this.plats[i]).subscribe(
  //           res => {
  //             console.log(res);
  //           }
  //         );
  //       }

  //       this.desserts = this.menuForm.value.listDesserts;
  //       for (let i = 0; i < this.desserts.length; i++) {
  //         console.log(this.desserts[i]);
  //         this.menuService.addProductToMenu(this.menuId, this.desserts[i]).subscribe(
  //           res => {
  //             console.log(res);
  //           }
  //         );
  //       }

  //       this.garnitures = this.menuForm.value.listGarnitures;
  //       for (let i = 0; i < this.garnitures.length; i++) {
  //         console.log(this.garnitures[i]);
  //         this.menuService.addProductToMenu(this.menuId, this.garnitures[i]).subscribe(
  //           res => {
  //             console.log(res);
  //           }
  //         );
  //       }

  //     }
  //   )

  //   // const Toast = Swal.mixin({
  //   //   toast: true,
  //   //   position: 'top-end',
  //   //   showConfirmButton: false,
  //   //   timer: 4000,
  //   //   timerProgressBar: true,
  //   //   didOpen: (toast) => {
  //   //     toast.addEventListener('mouseenter', Swal.stopTimer)
  //   //     toast.addEventListener('mouseleave', Swal.resumeTimer)
  //   //   }
  //   // })

  //   // Toast.fire({
  //   //   icon: 'success',
  //   //   title: ' Creation du menu en cours'
  //   // }).then(()=>{
  //   //   this.router.navigateByUrl("/menu")
  //   // })

  // }

  onChange(attribut: string, isChecked: boolean) {
    const emailFormArray = <FormArray>this.menuForm.controls.listEntrees;

    if (isChecked) {
      emailFormArray.push(new FormControl(attribut));
    } else {
      let index = emailFormArray.controls.findIndex((x) => x.value == attribut);
      emailFormArray.removeAt(index);
    }
  }

  onChange1(attribut: string, isChecked: boolean) {
    const emailFormArray = <FormArray>this.menuForm.controls.listPlats;

    if (isChecked) {
      emailFormArray.push(new FormControl(attribut));
    } else {
      let index = emailFormArray.controls.findIndex((x) => x.value == attribut);
      emailFormArray.removeAt(index);
    }
  }

  onChange3(attribut: string, isChecked: boolean) {
    const emailFormArray = <FormArray>this.menuForm.controls.listDesserts;

    if (isChecked) {
      emailFormArray.push(new FormControl(attribut));
    } else {
      let index = emailFormArray.controls.findIndex((x) => x.value == attribut);
      emailFormArray.removeAt(index);
    }
  }

  onChange2(attribut: string, isChecked: boolean) {
    const emailFormArray = <FormArray>this.menuForm.controls.listGarnitures;

    if (isChecked) {
      emailFormArray.push(new FormControl(attribut));
    } else {
      let index = emailFormArray.controls.findIndex((x) => x.value == attribut);
      emailFormArray.removeAt(index);
    }
  }

  goToAddProduct(id: number) {
    this.router.navigateByUrl(`admin/addproduct/2/${id}`);
  }
}
