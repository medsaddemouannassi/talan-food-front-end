import {AddMenuComponent} from './add-menu.component';
import {FormBuilder} from "@angular/forms";
import {of} from "rxjs";
import Swal from "sweetalert2";

describe('AddMenuComponent', () => {
  let fixture: AddMenuComponent;
  let formBuilderMock: FormBuilder;
  let productServiceMock: any;
  let menuServiceMock: any;
  let routerMock: any;
  let products: any;

  beforeEach(() => {
    products = [
      {
        "id": 1,
        "name": "koseksi",
        "description": "bnine",
        "quantity": 9,
        "price": 14.5,
        "image": "image",
        "displayed": true,
        "category": {
          "id": 1,
          "name": "pp"
        }
      },
      {
        "id": 11,
        "name": "koseksi",
        "description": "bnine",
        "quantity": 9,
        "price": 14.5,
        "image": "image",
        "displayed": true,
        "category": {
          "id": 2,
          "name": "pp"
        }
      },
      {
        "id": 111,
        "name": "koseksi",
        "description": "bnine",
        "quantity": 9,
        "price": 14.5,
        "image": "image",
        "displayed": true,
        "category": {
          "id": 3,
          "name": "pp"
        }
      },
      {
        "id": 1111,
        "name": "koseksi",
        "description": "bnine",
        "quantity": 9,
        "price": 14.5,
        "image": "image",
        "displayed": true,
        "category": {
          "id": 4,
          "name": "pp"
        }
      }
    ]
    formBuilderMock = new FormBuilder();
    productServiceMock = {
      getProductByCat: jest.fn(() => of()),
      getProductsForMenu: jest.fn(() => of(products))
    }
    menuServiceMock = {
      addMenu: jest.fn()
    }
    routerMock = {
      navigateByUrl: jest.fn()
    }
    fixture = new AddMenuComponent(
      formBuilderMock,
      productServiceMock,
      menuServiceMock,
      routerMock,
    )
  });

  afterEach(() => {
    jest.clearAllMocks()
  })

  describe('Test: ngOnInit', () => {
    it('should initialize menu form', () => {
      fixture.ngOnInit();
      expect(fixture.menuForm.value.dateMenu).toEqual("");
    });
    it('should get "salades"', () => {
      jest.spyOn(productServiceMock, "getProductByCat").mockReturnValue(of([products[0]]));
      fixture.ngOnInit();
      expect(fixture.entreesDispo[0].category.id).toEqual(1);
    });
    it('should get "plats"', () => {
      jest.spyOn(productServiceMock, "getProductByCat").mockReturnValue(of([products[1]]));
      fixture.ngOnInit();
      expect(fixture.platsDispo[0].category.id).toEqual(2);
    });
    it('should get "garnitures"', () => {
      jest.spyOn(productServiceMock, "getProductByCat").mockReturnValue(of([products[2]]));
      fixture.ngOnInit();
      expect(fixture.garnitureDispo[0].category.id).toEqual(3);
    });
    it('should get "desserts"', () => {
      jest.spyOn(productServiceMock, "getProductByCat").mockReturnValue(of([products[3]]));
      fixture.ngOnInit();
      expect(fixture.dessertDispo[0].category.id).toEqual(4);
    });
    it('should get "productsDispo"', () => {
      fixture.ngOnInit();
      expect(fixture.productsDispo).toEqual(products);
    });
  });

  describe('Test: addMenu', () => {
    it('should add menu', () => {
      fixture.ngOnInit();
      fixture.menuForm.value.dateMenu = '2022-07-07'
      fixture.menuForm.value.listEntrees = [products[0]]
      fixture.menuForm.value.listPlats = [products[1]]
      fixture.menuForm.value.listDesserts = [products[2]]
      fixture.menuForm.value.listGarnitures = [products[3]]
      fixture.addMenu();
      Swal.clickConfirm();
      jest.spyOn(menuServiceMock, 'addMenu').mockReturnValue(of());
      setTimeout(() => {
        expect(menuServiceMock.addMenu).toHaveBeenCalled();
      });
    });
    it('should not add menu', () => {
      fixture.ngOnInit();
      fixture.menuForm.value.dateMenu = '2022-07-07'
      fixture.menuForm.value.listEntrees = [products[0]]
      fixture.menuForm.value.listPlats = [products[1]]
      fixture.menuForm.value.listDesserts = [products[2]]
      fixture.menuForm.value.listGarnitures = [products[3]]
      fixture.addMenu();
      Swal.clickDeny();
    });
  });

  describe('Test: onChange', () => {
    it('should add product to menu', () => {
      fixture.ngOnInit();
      fixture.onChange("7", true);
      expect(fixture.menuForm.value.listEntrees).toContain("7")
    });
    it('should not add product to menu', () => {
      fixture.ngOnInit();
      fixture.onChange("7", true);
      fixture.onChange("17", true);
      fixture.onChange("27", true);
      fixture.onChange("7", false);
      expect(fixture.menuForm.value.listEntrees).not.toContain("7")
    });
  });

  describe('Test: onChange1', () => {
    it('should add product to menu', () => {
      fixture.ngOnInit();
      fixture.onChange1("7", true);
      expect(fixture.menuForm.value.listPlats).toContain("7")
    });
    it('should not add product to menu', () => {
      fixture.ngOnInit();
      fixture.onChange1("7", true);
      fixture.onChange1("17", true);
      fixture.onChange1("27", true);
      fixture.onChange1("7", false);
      expect(fixture.menuForm.value.listPlats).not.toContain("7")
    });
  });

  describe('Test: onChange2', () => {
    it('should add product to menu', () => {
      fixture.ngOnInit();
      fixture.onChange2("7", true);
      expect(fixture.menuForm.value.listGarnitures).toContain("7")
    });
    it('should not add product to menu', () => {
      fixture.ngOnInit();
      fixture.onChange2("7", true);
      fixture.onChange2("17", true);
      fixture.onChange2("27", true);
      fixture.onChange2("7", false);
      expect(fixture.menuForm.value.listGarnitures).not.toContain("7")
    });
  });

  describe('Test: onChange3', () => {
    it('should add product to menu', () => {
      fixture.ngOnInit();
      fixture.onChange3("7", true);
      expect(fixture.menuForm.value.listDesserts).toContain("7")
    });
    it('should not add product to menu', () => {
      fixture.ngOnInit();
      fixture.onChange3("7", true);
      fixture.onChange3("17", true);
      fixture.onChange3("27", true);
      fixture.onChange3("7", false);
      expect(fixture.menuForm.value.listDesserts).not.toContain("7")
    });
  });

  describe('Test: goToAddProduct', () => {
    it('should add product to menu', () => {
      fixture.ngOnInit();
      fixture.goToAddProduct(7);
      expect(routerMock.navigateByUrl).toBeCalledWith(`admin/addproduct/2/7`)
    });
  });
});
