import { HeaderComponent } from './header.component';
import { of } from 'rxjs';
describe('HeaderComponent', () => {
  let fixture: HeaderComponent;
  let authServiceMock: any;
  let routerMock: any;
  let reservationServiceMock: any;
  let subscriptionServiceMock: any;
  let userServiceMock: any;
  let subscriptions: any;
  let subscription = { id: 22 };

  beforeEach(async () => {
    let user = {
      email: 'user@talan.com',
      firstName: 'user',
      id: 17,
      lastName: 'user',
      password: null,
      phone: '22222222',
      role: { id: 2, name: 'USER' },
      notificationsHidden: false,
    };
    authServiceMock = {
      isUserLoggedIn: jest.fn(() => true),
      getUserFromLocalCache: jest.fn(() => user),
      getRoles: jest.fn(() => ['USER']),
      logOut: jest.fn(),
    };

    userServiceMock = {
      getUserByid: jest.fn(() => of(user)),
      changeHiddenNotifications: jest.fn(() => of(user)),
    };

    reservationServiceMock = {
      getNotifications: jest.fn(() => of(2)),
    };

    subscriptionServiceMock = {
      getNotifications: jest.fn(() => of(3)),
      getSubscriptions: jest.fn(() => of(subscriptions)),
      changeToSeen: jest.fn(() => of(subscription)),
    };

    routerMock = { navigateByUrl: jest.fn() };

    fixture = new HeaderComponent(
      authServiceMock,
      reservationServiceMock,
      subscriptionServiceMock,
      userServiceMock,
      routerMock
    );
    fixture.ngOnInit();
  });

  describe('Test: ngOnInit', () => {
    it('should initialize attributes', () => {
      expect(fixture.isUserLogged).toEqual(true);
      expect(fixture.loggedUser).toEqual('user user');
      expect(fixture.role).toEqual('USER');
      expect(subscriptionServiceMock.getSubscriptions).toBeCalled();
      expect(subscriptionServiceMock.getNotifications).toBeCalled();
      expect(userServiceMock.getUserByid).toBeCalled();
      expect(fixture.subscriptions).toEqual(subscriptions);
      expect(fixture.notSeenUser).toEqual(3);
      expect(fixture.hidden).toEqual(false);
    });
  });

  describe('Test: logout', () => {
    it('should logout', () => {
      fixture.logout();
      const spyLogout = jest.spyOn(authServiceMock, 'logOut');
      expect(authServiceMock.logOut());
      expect(spyLogout).toBeCalled();
    });
  });

  describe('Test: clearNotifications', () => {
    it('should  clearNotifications', () => {
      fixture.clearNotifications();
      expect(userServiceMock.changeHiddenNotifications).toBeCalled();
    });
  });

  describe('Test: isSeenUser', () => {
    it('should  change subscription to seen', () => {
      fixture.isSeenUser(subscription);
      expect(subscriptionServiceMock.changeToSeen).toBeCalledWith(22);
      expect(routerMock.navigateByUrl).toBeCalledWith('menu');
    });
  });
});
