import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdminComponent } from './components/admin/admin.component';
import { RegisterComponent } from './components/register/register.component';
import { LoginComponent } from './components/login/login.component';
import { Error403Component } from './components/error403/error403.component';
import { Error404Component } from './components/error404/error404.component';
import { AuthenticationGuard } from './helpers/authentication.guard';
import { RoleGuard } from './helpers/role.guard';
import { AddProductComponent } from './components/add-product/add-product.component';
import { HomeComponent } from './components/home/home.component';
import { MenuComponent } from './components/menu/menu.component';
import { AddInteractionComponent } from './components/add-interaction/add-interaction.component';
import { ShowInteractionComponent } from './components/show-interaction/show-interaction.component';
import { ShowMyreservationComponent } from './components/show-myreservation/show-myreservation.component';
import { OrdersComponent } from './components/orders/orders.component';
import { ShowAllReservationsComponent } from './components/show-all-reservations/show-all-reservations.component';
import { ProductsComponent } from './components/products/products.component';
import { AddMenuComponent } from './components/add-menu/add-menu.component';
import { OrderProductComponent } from './components/order-product/order-product.component';
import { EmailresponseComponent } from './components/emailresponse/emailresponse.component';
import { ProductDetailsComponent } from './components/product-details/product-details.component';
import { EditProductComponent } from './components/edit-product/edit-product.component';
import { AuthGuard } from './helpers/auth.guard';
import { EditMenuItemComponent } from './components/edit-menu-item/edit-menu-item.component';
import { KpiComponent } from './kpi/kpi.component';
import { StaticPriceComponent } from './static-price/static-price.component';
import { IncomeUserComponent } from './income-user/income-user.component';

const routes: Routes = [
  { path: '', redirectTo: '/index', pathMatch: 'full' },
  {
    path: '',
    component: AdminComponent,
    children: [
      { path: 'products', component: OrderProductComponent },
      { path: 'products/:id', component: ProductDetailsComponent },
      { path: 'index', component: HomeComponent },
      { path: 'menu', component: MenuComponent },
      {
        path: 'getReservationByUserId',
        component: ShowMyreservationComponent,
        canActivate: [AuthenticationGuard, RoleGuard],
        data: { role: 'USER' },
      },
      {
        path: 'addIntercation',
        component: AddInteractionComponent,
        canActivate: [AuthenticationGuard, RoleGuard],
        data: { role: 'USER' },
      },
      {
        path: 'ordersreservation/:id',
        component: OrdersComponent,
        canActivate: [AuthenticationGuard, RoleGuard],
        data: { role: 'USER' },
      },
    ],
  },
  {
    path: 'admin',
    component: AdminComponent,

    canActivate: [AuthenticationGuard, RoleGuard],
    data: { role: 'ADMIN' },
    children: [
      { path: 'addproduct/:origin/:id', component: AddProductComponent },
      { path: 'products/:id', component: EditProductComponent },
      { path: 'showIntercation', component: ShowInteractionComponent },
      { path: 'allreservation', component: ShowAllReservationsComponent },
      { path: 'emailresponse/:id', component: EmailresponseComponent },
      { path: 'products', component: ProductsComponent },
      { path: 'add-menu', component: AddMenuComponent },
      { path: 'menu-item/:id', component: EditMenuItemComponent },
      { path: 'kpi/reservations', component: KpiComponent },
      { path: 'kpi/incomes', component: StaticPriceComponent },
      { path: 'kpi/incomes/user', component: IncomeUserComponent },
    ],
  },
  {
    path: 'page-register',
    component: RegisterComponent,
    canActivate: [AuthGuard],
  },
  { path: 'page-login', component: LoginComponent, canActivate: [AuthGuard] },
  { path: 'page-error-403', component: Error403Component },
  { path: 'page-error-404', component: Error404Component },
  { path: '**', component: Error404Component },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { scrollPositionRestoration: 'enabled' }),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule {}
