import {Category} from "./Category";

export class Product {
  id!: number;
  name!: string;
  description!: string;
  quantity!: number;
  price!: number;
  image!: string;
  displayed!: boolean;
  Category!: Category
}
