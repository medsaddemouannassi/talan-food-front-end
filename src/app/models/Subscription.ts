import { Product } from './Product';
import { User } from './User';

export class Subscription {
  id!: number;
  activated!: boolean;
  menuDate!: Date;
  seen!: boolean;
  product!: Product;
  user!: User;
}
