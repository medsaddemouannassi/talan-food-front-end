import { User } from './User';

export class Reservation {
  id!: number;
  price!: number;
  user!: User;
  date!: Date;
  seen!: boolean;
}
