import { User } from "./User";

export class Interactions {
    id!:number;
    type!: string;
    description!:String;
    value!:number;
    user!: User;
}
