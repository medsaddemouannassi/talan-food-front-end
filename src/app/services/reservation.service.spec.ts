import { of } from 'rxjs';

import { ReservationService } from './reservation.service';
import { HttpParams } from '@angular/common/http';

describe('ReservationService', () => {
  let service: ReservationService;
  let httpclientSpy: any;
  beforeEach(() => {
    httpclientSpy = {
      get: jest.fn(),
      delete: jest.fn(),
      post: jest.fn(),
    };
    service = new ReservationService(httpclientSpy);
  });

  it('should test getAllReservations()', () => {
    const rest = 'hey hey';
    const url = 'http://localhost:8090/api/reservations';
    jest.spyOn(httpclientSpy, 'get').mockReturnValue(of(rest));
    service.getAllReservations();
    expect(httpclientSpy.get).toBeCalledTimes(1);
    expect(httpclientSpy.get).toHaveBeenLastCalledWith(url);
  });

  it('should test deleteReservationById()', () => {
    const reservationId = 1;
    const url = 'http://localhost:8090/api/reservations/annulation/1';
    service.deleteReservationById(1);
    expect(httpclientSpy.delete).toHaveBeenLastCalledWith(url);
  });

  it('should test addReservation()', () => {
    const url = 'http://localhost:8090/api/reservations';
    let reservation: any;
    service.addReservation(reservation);
    expect(httpclientSpy.post).toHaveBeenLastCalledWith(url, reservation);
  });

  it('should test confirmReservation()', () => {
    const url = 'http://localhost:8090/api/reservations/confirm';
    let reservation: any;
    service.confirmReservation(reservation);
    expect(httpclientSpy.post).toHaveBeenLastCalledWith(url, reservation);
  });

  it('should test getAllReservationByDate()', () => {
    const url = 'http://localhost:8090/api/reservations/date';
    let reservation: any;
    let date: any;
    let params = new HttpParams();
    params = params.append('date', date);
    service.getAllReservationByDate(date);
    expect(httpclientSpy.get).toHaveBeenLastCalledWith(url, { params: params });
  });
  it('should test getAllReservationByDateAsc()', () => {
    service.getAllReservationsByDateAsc();
    expect(httpclientSpy.get).toBeCalledWith(
      `http://localhost:8090/api/reservations/static`
    );
  });
  it('should test getAllReservationByMonth()', () => {
    service.getAllReservationsByMonth();
    expect(httpclientSpy.get).toBeCalledWith(
      `http://localhost:8090/api/reservations/static/month`
    );
  });
  it('should test getAllReservationByYear()', () => {
    service.getAllReservationsByYear();
    expect(httpclientSpy.get).toBeCalledWith(
      `http://localhost:8090/api/reservations/static/year`
    );
  });
  it('should test getAllIncomesYear()', () => {
    service.getAllIncomesByDate();
    expect(httpclientSpy.get).toBeCalledWith(
      `http://localhost:8090/api/reservations/incomes`
    );
  });

  it('should test getAllIncomesYear()', () => {
    service.getAllIncomesByMonth();
    expect(httpclientSpy.get).toBeCalledWith(
      `http://localhost:8090/api/reservations/incomes/month`
    );
  });
  it('should test getAllIncomesYear()', () => {
    service.getAllIncomesByYear();
    expect(httpclientSpy.get).toBeCalledWith(
      `http://localhost:8090/api/reservations/incomes/year`
    );
  });
  it('should test getAllIncomesYear()', () => {
    service.getAllIncomesByUser();
    expect(httpclientSpy.get).toBeCalledWith(
      `http://localhost:8090/api/reservations/incomes/user`
    );
  });
});
