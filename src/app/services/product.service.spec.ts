import { ProductService } from './product.service';

describe('ProductService', () => {
  let service: ProductService;
  let httpClientSpy: any;

  beforeEach(() => {
    httpClientSpy = {
      get: jest.fn(),
      post: jest.fn(),
      delete: jest.fn(),
    };
    service = new ProductService(httpClientSpy);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('Test: getProducts', () => {
    it('should get products', () => {
      service.getProducts();
      expect(httpClientSpy.get).toBeCalledWith(`${service.Url}`);
    });
  });

  describe('Test: addProduct', () => {
    it('should add product', () => {
      let formData = new FormData();
      service.addProduct(formData);
      expect(httpClientSpy.post).toBeCalledWith(
        `${service.Url}/new/product`,
        formData
      );
    });
  });

  describe('Test: updateProfileImage', () => {
    it('should update profile image', () => {
      let formData = new FormData();
      service.updateProfileImage(formData);
      expect(httpClientSpy.post).toBeCalledWith(
        `${service.Url}/image`,
        formData
      );
    });
  });

  describe('Test: getProduct', () => {
    it('should get product', () => {
      let any: any;
      service.getProduct(any);
      expect(httpClientSpy.get).toBeCalledWith(`${service.Url}/product/${any}`);
    });
  });

  describe('Test: getProductByCat', () => {
    it('should get product by category', () => {
      let id: any;
      service.getProductByCat(id);
      expect(httpClientSpy.get).toBeCalledWith(`${service.Url}/catid/${id}`);
    });
  });

  describe('Test: updateProduct', () => {
    it('should update product', () => {
      let formData = new FormData();
      service.updateProduct(formData);
      expect(httpClientSpy.post).toBeCalledWith(
        `${service.Url}/product`,
        formData
      );
    });
  });

  describe('Test: deleteProduct', () => {
    it('should delete product', () => {
      let id: any;
      service.deleteProduct(id);
      expect(httpClientSpy.delete).toBeCalledWith(
        `http://localhost:8090/api/products/${id}`
      );
    });
  });

  describe('Test: updateQuantity', () => {
    it('should update quantity', () => {
      let id: any;
      let quantity: any;
      service.updateQuantity(id, quantity);
      expect(httpClientSpy.post).toBeCalledWith(
        `http://localhost:8090/api/products/quantity/${id}/${quantity}`,
        null
      );
    });
  });

  describe('Test: updateProductQuantity', () => {
    it('should update product quantity', () => {
      let id: any;
      let quantity: any;
      service.updateProductQuantity(id, quantity);
      expect(httpClientSpy.post).toBeCalledWith(
        `${service.Url}/quantity/${id}/${quantity}`,
        null
      );
    });
  });

  describe('Test: getProductsForMenu', () => {
    it('should get products for menu', () => {
      service.getProductsForMenu();
      expect(httpClientSpy.get).toBeCalledWith(`${service.Url}`);
    });
  });

  describe('Test: getRating', () => {
    it('should get rating', () => {
      service.getRating(7);
      expect(httpClientSpy.get).toBeCalledWith(
        `http://localhost:8090/api/products/ratings/7/`
      );
    });
  });

  describe('Test: rateProduct', () => {
    it('should rate product', () => {
      let any = 7;
      service.rateProduct(any);
      expect(httpClientSpy.post).toBeCalledWith(
        'http://localhost:8090/api/products/ratings',
        any
      );
    });
  });

  it('should test rateProductByUser()', () => {
    const idProduct = 1;
    const idUser = 1;
    service.rateProductByUser(idUser, idProduct);
    expect(httpClientSpy.get).toBeCalledWith(
      `http://localhost:8090/api/products/ratings/${idUser}/${idProduct}`
    );
  });
});
