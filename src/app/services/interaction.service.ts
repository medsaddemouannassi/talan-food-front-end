import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class InteractionService {


  baseUrl="http://localhost:8090"
  constructor(private httpclient :HttpClient) { }

  public showReclamations(): Observable<any>{
    return this.httpclient.get<any>(`${this.baseUrl}/api/interactions/reclamations`);
  }

  public showSuggestions(): Observable<any>{
    return this.httpclient.get<any>(`${this.baseUrl}/api/interactions/suggestions`);
  }

  public showNotes():Observable<any>{
    return this.httpclient.get<any>(`${this.baseUrl}/api/interactions/notes`)
  }

  public deleteeInteraction(interactionId:number): Observable<void>{
    return this.httpclient.delete<void>(`${this.baseUrl}/api/interactions/${interactionId}`);
  }



  public addInteraction(interaction:FormGroup ): Observable<any>{
    return this.httpclient.post<any>(`${this.baseUrl}/api/interactions`,interaction);
  }


  public answerClient(idRec:number, message:string): Observable<any>{
    let params = new HttpParams();
    params = params.append('message', message);
    return this.httpclient.get(`${this.baseUrl}/api/interactions/reclamation/response/${idRec}`,{params: params});
  }

  public answerToSuggesion(idSug:number): Observable<any>{
    
    return this.httpclient.get(`${this.baseUrl}/api/interactions/suggession/response/${idSug}`);
  }



}


