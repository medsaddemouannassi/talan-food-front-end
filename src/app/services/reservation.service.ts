import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Reservation } from '../models/Reservation';

@Injectable({
  providedIn: 'root',
})
export class ReservationService {
  baseUrl = 'http://localhost:8090';

  constructor(private httpclient: HttpClient) {}

  public getReservationByUserId(iduser: number, page: number): Observable<any> {
    return this.httpclient.get<any>(
      `${this.baseUrl}/api/reservations/user/${iduser}/${page}`
    );
  }

  public deleteReservationById(reservationId: Number) {
    return this.httpclient.delete(
      `${this.baseUrl}/api/reservations/annulation/${reservationId}`
    );
  }

  public getAllReservations() {
    return this.httpclient.get<any>(`${this.baseUrl}/api/reservations`);
  }

  public addReservation(reservation: any) {
    return this.httpclient.post(
      `${this.baseUrl}/api/reservations`,
      reservation
    );
  }

  public confirmReservation(reservation: any) {
    return this.httpclient.post(
      `${this.baseUrl}/api/reservations/confirm`,
      reservation
    );
  }

  public getAllReservationByDate(date: any) {
    let params = new HttpParams();
    params = params.append('date', date);
    return this.httpclient.get<any>(`${this.baseUrl}/api/reservations/date`, {
      params: params,
    });
  }
  public getAllReservationsByDateAsc() {
    return this.httpclient.get<any>(`${this.baseUrl}/api/reservations/static`);
  }

  public getNotifications(): Observable<number> {
    return this.httpclient.get<number>(
      `${this.baseUrl}/api/reservations/countNotSeen`
    );
  }

  public changeToSeen(id: number): Observable<Reservation> {
    return this.httpclient.patch<Reservation>(
      `${this.baseUrl}/api/reservations/seen/${id}`,
      {}
    );
  }

  public getNotSeenReservations(): Observable<[Reservation]> {
    return this.httpclient.get<[Reservation]>(
      `${this.baseUrl}/api/reservations`
    );
  }
  public getAllReservationsByMonth() {
    return this.httpclient.get<any>(
      `${this.baseUrl}/api/reservations/static/month`
    );
  }
  public getAllReservationsByYear() {
    return this.httpclient.get<any>(
      `${this.baseUrl}/api/reservations/static/year`
    );
  }
  public getAllIncomesByDate() {
    return this.httpclient.get<any>(`${this.baseUrl}/api/reservations/incomes`);
  }
  public getAllIncomesByMonth() {
    return this.httpclient.get<any>(
      `${this.baseUrl}/api/reservations/incomes/month`
    );
  }
  public getAllIncomesByYear() {
    return this.httpclient.get<any>(
      `${this.baseUrl}/api/reservations/incomes/year`
    );
  }
  public getAllIncomesByUser() {
    return this.httpclient.get<any>(
      `${this.baseUrl}/api/reservations/incomes/user`
    );
  }
}
