import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IncomeUserComponent } from './income-user.component';

describe('IncomeUserComponent', () => {
  let component: IncomeUserComponent;
  let fixture: ComponentFixture<IncomeUserComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IncomeUserComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IncomeUserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
