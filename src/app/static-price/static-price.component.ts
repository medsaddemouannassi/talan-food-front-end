import { getLocaleDateFormat } from '@angular/common';
import { Component, OnInit, ViewChild } from '@angular/core';

import {
  ApexAxisChartSeries,
  ApexChart,
  ChartComponent,
  ApexDataLabels,
  ApexPlotOptions,
  ApexYAxis,
  ApexTitleSubtitle,
  ApexAnnotations,
  ApexStroke,
  ApexGrid,
  ApexXAxis,
  ApexFill,
} from 'ng-apexcharts';
import { ReservationService } from '../services/reservation.service';

export type ChartOptions = {
  series?: ApexAxisChartSeries | any;
  chart?: ApexChart | any;
  dataLabels?: ApexDataLabels | any;
  plotOptions?: ApexPlotOptions | any;
  yaxis?: ApexYAxis | any;
  xaxis?: ApexXAxis | any;
  fill?: ApexFill | any;
  title?: ApexTitleSubtitle | any;
  annotations?: ApexAnnotations | any;
  stroke?: ApexStroke | any;
  grid?: ApexGrid | any;
};
@Component({
  selector: 'app-static-price',
  templateUrl: './static-price.component.html',
  styleUrls: ['./static-price.component.css'],
})
export class StaticPriceComponent implements OnInit {
  @ViewChild('chart') chart!: ChartComponent;
  public chartOptions: Partial<ChartOptions>;
  public chartOptions2: Partial<ChartOptions>;
  public chartOptions3: Partial<ChartOptions>;
  public chartOptions4: Partial<ChartOptions>;
  xAxe: any[] = [];
  yAxe: any[] = [];
  xAxe2: any[] = [];
  yAxe2: any[] = [];
  xAxe3: any[] = [];
  yAxe3: any[] = [];
  xAxe4: any[] = [];
  yAxe4: any[] = [];

  constructor(private reservationService: ReservationService) {
    this.chartOptions = {
      series: [
        {
          name: 'Revenus',
          data: this.yAxe,
        },
      ],
      chart: {
        height: 350,
        type: 'bar',
        innerWidth: 5,
      },
      plotOptions: {
        bar: {
          dataLabels: {
            position: 'top', // top, center, bottom
          },
        },
      },
      dataLabels: {
        enabled: true,
        formatter: function (val: any) {
          return val + 'DT';
        },
        offsetY: -20,
        style: {
          fontSize: '12px',
          colors: ['#304758'],
        },
      },

      xaxis: {
        categories: this.xAxe,
        position: 'top',
        labels: {
          offsetY: -18,
        },
        axisBorder: {
          show: false,
        },
        axisTicks: {
          show: false,
        },
        crosshairs: {
          fill: {
            type: 'gradient',
            gradient: {
              colorFrom: '#D8E3F0',
              colorTo: '#BED1E6',
              stops: [0, 100],
              opacityFrom: 0.4,
              opacityTo: 0.5,
            },
          },
        },
        tooltip: {
          enabled: true,
          offsetY: -35,
        },
      },
      fill: {
        type: 'gradient',
        gradient: {
          shade: 'light',
          type: 'horizontal',
          shadeIntensity: 0.25,
          gradientToColors: undefined,
          inverseColors: true,
          opacityFrom: 1,
          opacityTo: 1,
          stops: [50, 0, 100, 100],
        },
      },
      yaxis: {
        axisBorder: {
          show: false,
        },
        axisTicks: {
          show: false,
        },
        labels: {
          show: false,
          formatter: function (val: any) {
            return val + 'DT';
          },
        },
      },
      title: {
        text: 'Les revenus journaliers',
        offsetY: 330,
        align: 'center',
        style: {
          color: '#444',
        },
      },
    };

    this.chartOptions2 = {
      series: [
        {
          name: 'Revenus',
          data: this.yAxe2,
        },
      ],
      chart: {
        height: 350,
        type: 'bar',
        innerWidth: 5,
      },
      plotOptions: {
        bar: {
          dataLabels: {
            position: 'top', // top, center, bottom
          },
        },
      },
      dataLabels: {
        enabled: true,
        formatter: function (val: any) {
          return val + 'DT';
        },
        offsetY: -20,
        style: {
          fontSize: '12px',
          colors: ['#304758'],
        },
      },

      xaxis: {
        categories: this.xAxe2,
        position: 'top',
        labels: {
          offsetY: -18,
        },
        axisBorder: {
          show: false,
        },
        axisTicks: {
          show: false,
        },
        crosshairs: {
          fill: {
            type: 'gradient',
            gradient: {
              colorFrom: '#D8E3F0',
              colorTo: '#BED1E6',
              stops: [0, 100],
              opacityFrom: 0.4,
              opacityTo: 0.5,
            },
          },
        },
        tooltip: {
          enabled: true,
          offsetY: -35,
        },
      },
      fill: {
        type: 'gradient',
        gradient: {
          shade: 'light',
          type: 'horizontal',
          shadeIntensity: 0.25,
          gradientToColors: undefined,
          inverseColors: true,
          opacityFrom: 1,
          opacityTo: 1,
          stops: [50, 0, 100, 100],
        },
      },
      yaxis: {
        axisBorder: {
          show: false,
        },
        axisTicks: {
          show: false,
        },
        labels: {
          show: false,
          formatter: function (val: any) {
            return val + 'DT';
          },
        },
      },
      title: {
        text: `Les revenus par mois de l'année ${new Date().getFullYear()}`,
        offsetY: 330,
        align: 'center',
        style: {
          color: '#444',
        },
      },
    };

    this.chartOptions3 = {
      series: [
        {
          name: 'Revenus',
          data: this.yAxe3,
        },
      ],
      chart: {
        height: 350,
        type: 'bar',
        innerWidth: 5,
      },
      plotOptions: {
        bar: {
          dataLabels: {
            position: 'top', // top, center, bottom
          },
        },
      },
      dataLabels: {
        enabled: true,
        formatter: function (val: any) {
          return val + 'DT';
        },
        offsetY: -20,
        style: {
          fontSize: '12px',
          colors: ['#304758'],
        },
      },

      xaxis: {
        categories: this.xAxe3,
        position: 'top',
        labels: {
          offsetY: -18,
        },
        axisBorder: {
          show: false,
        },
        axisTicks: {
          show: false,
        },
        crosshairs: {
          fill: {
            type: 'gradient',
            gradient: {
              colorFrom: '#D8E3F0',
              colorTo: '#BED1E6',
              stops: [0, 100],
              opacityFrom: 0.4,
              opacityTo: 0.5,
            },
          },
        },
        tooltip: {
          enabled: true,
          offsetY: -35,
        },
      },
      fill: {
        type: 'gradient',
        gradient: {
          shade: 'light',
          type: 'horizontal',
          shadeIntensity: 0.25,
          gradientToColors: undefined,
          inverseColors: true,
          opacityFrom: 1,
          opacityTo: 1,
          stops: [50, 0, 100, 100],
        },
      },
      yaxis: {
        axisBorder: {
          show: false,
        },
        axisTicks: {
          show: false,
        },
        labels: {
          show: false,
          formatter: function (val: any) {
            return val + 'DT';
          },
        },
      },
      title: {
        text: 'Les revenus par an',
        offsetY: 330,
        align: 'center',
        style: {
          color: '#444',
        },
      },
    };

    this.chartOptions4 = {
      series: [
        {
          name: 'Total des achats',
          data: this.yAxe4,
        },
      ],
      chart: {
        type: 'bar',
        height: 350,
      },
      plotOptions: {
        bar: {
          horizontal: true,
        },
      },
      dataLabels: {
        enabled: false,
      },
      xaxis: {
        categories: this.xAxe4,
      },
    };
  }

  ngOnInit(): void {
    this.getIncomes();
    this.getIncomesMonth();
    this.getIncomesYear();
    this.getIncomesUser();
  }
  getIncomes() {
    this.reservationService.getAllIncomesByDate().subscribe((response) => {
      response.forEach((element: any) => {
        this.xAxe.push(element.date);
        this.yAxe.push(element.price);
      });
    });
  }
  getIncomesMonth() {
    this.reservationService.getAllIncomesByMonth().subscribe((response) => {
      response.forEach((element: any) => {
        this.xAxe2.push(element.date);
        this.yAxe2.push(element.price);
      });
    });
  }

  getIncomesYear() {
    this.reservationService.getAllIncomesByYear().subscribe((response) => {
      response.forEach((element: any) => {
        this.xAxe3.push(element.date);
        this.yAxe3.push(element.price);
      });
    });
  }

  getIncomesUser() {
    this.reservationService.getAllIncomesByUser().subscribe((response) => {
      response.forEach((element: any) => {
        this.xAxe4.push(element.user.firstName);
        console.log(this.xAxe4);
        this.yAxe4.push(element.price);
        console.log(this.yAxe4);
      });
    });
  }
}
